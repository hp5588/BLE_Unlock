//
//  ServerCommunicator.h
//  BLE_Unlock
//
//  Created by Brian on 2015/8/17.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol serverResultDelegate <NSObject>

-(void)serverResponseWith:(NSInteger)response datas:(NSDictionary*)dataDict;

@end



@interface ServerCommunicator : NSObject<NSURLSessionDataDelegate>
-(id)init:(NSString*)ID password:(NSString*)password;
-(void)checkVerification;
-(void)aesKeyPush:(NSString *)ivKeyString bleUUID:(NSString*)bleUUID;
-(void)apnsTokenPushToServer:(NSString *)token;

//User ID can be nil
//Will parse ID with token if both given
-(void)fbTokenPushToServer:(NSString *)token;




@property id<serverResultDelegate> delegate;

@end