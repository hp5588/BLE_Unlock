//
//  ImageManagerViewController.m
//  BLE_Unlock
//
//  Created by Brian on 2015/1/22.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import "ImageManagerViewController.h"

@interface ImageManagerViewController ()
@property UIImagePickerController *imageController;
@property int state;

@end

@implementation ImageManagerViewController
@synthesize imageController;
@synthesize state;


- (void)viewDidLoad {
    state = 0;
    //[super viewDidLoad];
    

    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated{
    
    //Initial Controller
    
    if([UIImagePickerController isSourceTypeAvailable:
        UIImagePickerControllerSourceTypeCamera]){
        imageController = [[UIImagePickerController alloc]init];
        imageController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imageController.delegate = self;
        imageController.allowsEditing = YES;
        imageController.showsCameraControls =YES;
        //[self presentViewController:imageController animated:YES completion:nil];
    }
    if (!state) {
    }
    //Test Remove
    //[self.delegate imageControllerFinished];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{

    //Save Picture
    

    //Dismiss Picker
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //state = 1;
    //[self dismissViewControllerAnimated:YES completion:nil];
    [[self delegate]imageControllerFinished];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
