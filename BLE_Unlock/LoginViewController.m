//
//  LoginViewController.m
//  BLE_Unlock
//
//  Created by Brian on 2015/8/15.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import "LoginViewController.h"
#import "serverAPIDefine.h"
#import "remoteCollect.h"



@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *idTextField;
@property (weak, nonatomic) IBOutlet UITextField *passTextField;
@property  UIActivityIndicatorView *actIndView;
@property NSDictionary *returnedJsonDictionary;
@property (strong, nonatomic) IBOutlet UIView *pageView;
@property ServerCommunicator *server;
@property FBSDKLoginButton *loginButton;

@end

@implementation LoginViewController

@synthesize idTextField;
@synthesize passTextField;

@synthesize returnedJsonDictionary;

@synthesize actIndView;
@synthesize pageView;

@synthesize server;

@synthesize loginButton;

- (IBAction)loginAction:(UIButton *)sender {
    server = [[ServerCommunicator alloc]init:idTextField.text password:passTextField.text];
    server.delegate = self;
    [server checkVerification];
    
    /*
    CGRect iconRect = CGRectMake(100, 100, 200 , 200);
    actIndView  = [[UIActivityIndicatorView alloc]initWithFrame:iconRect];
    [[self view]addSubview:actIndView];
    [actIndView startAnimating];
    
    */
    
}


-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data{
    NSError *error;
    returnedJsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &error];
    //[self checkVerification];
    
    NSLog(@"Get Data");

}

- (void)viewDidLoad {
    [super viewDidLoad];
    server = [[ServerCommunicator alloc]init];
    server.delegate = self;
    
    loginButton = [[FBSDKLoginButton alloc] init];
    // Optional: Place the button in the center of your view.
    loginButton.delegate = self;
    loginButton.center = self.view.center;
    [self.view addSubview:loginButton];
    
    
    // Do any additional setup after loading the view.
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    if ([FBSDKAccessToken currentAccessToken]) {
        [self performSegueWithIdentifier:@"VERI_TO_REMOTE" sender:nil];
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)serverResponseWith:(NSInteger)response datas:(NSDictionary *)dataDict{
    switch (response) {
        case RESPTYPE_PASSVERI:{
            NSLog(@"Login Succeed");
            //Pass verification
            //Push APNS token
            NSString *token = [[NSUserDefaults standardUserDefaults]objectForKey:@"APNS_TOKEN"];
            [server apnsTokenPushToServer:token];
            
            //Sync AES Key
            
            dispatch_async(dispatch_get_main_queue(),^{
                [self performSegueWithIdentifier:@"VERI_TO_REMOTE" sender:nil];
            });
        }
            break;
        case RESPTYPE_ERR_KEYPUSH:
            //Key already exist or sever errror
            
        default:
            break;
    }
}
-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{
    if ([FBSDKAccessToken currentAccessToken]) {
        [self performSegueWithIdentifier:@"VERI_TO_REMOTE" sender:nil];
    }
    //[self notifyLoginAction];

}
-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{

}

-(void)notifyLoginAction{
    //Notify Login action
    NSDictionary *loginStatusDict = [[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithBool:TRUE],@"login", nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"LoginStatusChange" object:nil userInfo:loginStatusDict];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    // Get the new view controller using [segue destinationViewController].
    //remoteCollection *destServer = (remoteCollection*)[[segue destinationViewController]rootViewController];
    //[destServer setID:self.idTextField.text password:self.passTextField.text];
    
    
    
    NSString *tokenString = [[FBSDKAccessToken currentAccessToken]tokenString];
    [GlobalVariablesManager saveString:self.idTextField.text withName:@"ID"];
    [GlobalVariablesManager saveString:self.passTextField.text withName:@"Password"];
    [GlobalVariablesManager saveString:tokenString withName:@"fb_token"];
    
    if (tokenString){
        NSLog(@"%@",tokenString);
        [server fbTokenPushToServer:tokenString];
    }
    
    [self notifyLoginAction];


    // Pass the selected object to the new view controller.
}


@end
