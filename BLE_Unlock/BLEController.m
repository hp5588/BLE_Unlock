//
//  BLEController.m
//  BLE_Unlock
//
//  Created by Brian on 2014/12/3.
//  Copyright (c) 2014年 Brian. All rights reserved.
//


#import "BLEController.h"
#import "RemoteData.h"
#import "DataManager.h"
#import "remoteCollect.h"
@interface BLEController()
@property CBCentralManager *centManager;
@property BOOL searching;
@property NSMutableArray *pairedDevice;


@property NSTimer *autoUpdateTimer;

@property CBUUID *uuid_service;
@property CBUUID *uuid_char1;

@property BOOL BLE_ON;
@end

@implementation BLEController

@synthesize peripherals;
@synthesize centManager;
@synthesize searching;
@synthesize pairedDevice;
@synthesize retreivedPeripherials;

@synthesize autoUpdateTimer;

@synthesize uuid_service;
@synthesize uuid_char1;

@synthesize BLE_ON;

-(void)initController{
    
    //Initial Central Manager
    centManager =  [[CBCentralManager alloc]initWithDelegate:self queue:nil options:@{CBCentralManagerOptionShowPowerAlertKey:@1}];
    
    //Initial Object
    peripherals = [[NSMutableArray alloc]init];

    //Initiate Parameter
    searching = 0;
    uuid_service = [CBUUID UUIDWithString:@"FFF0"];
    uuid_char1 = [CBUUID UUIDWithString:@"FFF1"];
    BLE_ON = 0;
    
    //Initiate AppDelegate
     //    UIApplication *app = [UIApplication sharedApplication];
//    app.delegate = self;

//TODO:
    //Retrieve Remote Data
    
    pairedDevice = [[NSMutableArray alloc]initWithArray:[DataManager retrieveRemoteDatas]];
    if (!pairedDevice) {
        pairedDevice = [[NSMutableArray alloc]init];
    }
    
    for (RemoteData *data in pairedDevice) {
        data.connected = 0;
    }
    
    
    
    [self searchPeripheralWithTimeout:1];
    
    //Make Retrieve Remote Data operationable Object
    retreivedPeripherials = [[NSMutableArray alloc]init];
    for (RemoteData *data in pairedDevice) {
        CBPeripheral *peri ;
        if(data.UUIDstring !=nil){
            NSUUID *identifier = [[NSUUID alloc]initWithUUIDString:data.UUIDstring];
            NSArray *returnPeris =[centManager retrievePeripheralsWithIdentifiers:[NSArray arrayWithObject:identifier]];
            if ([returnPeris count])
                peri = [returnPeris objectAtIndex:0];
            
            if (peri)
                [retreivedPeripherials addObject:peri];

        }
    }
    
    
    [DataManager saveRemoteDatas:pairedDevice];
    
}
-(void)disableController{
    [centManager stopScan];
}
-(void)enableController{

}


-(void)autoConnectToPairedDevice{
    [[self centManager]scanForPeripheralsWithServices:nil options:nil];
    for (RemoteData *data in pairedDevice) {
        CBPeripheral *peri ;
        if(data.UUIDstring !=nil){
            NSUUID *identifier = [[NSUUID alloc]initWithUUIDString:data.UUIDstring];
            NSArray *returnPeris =[centManager retrievePeripheralsWithIdentifiers:[NSArray arrayWithObject:identifier]];
            if ([returnPeris count])
                peri = [returnPeris objectAtIndex:0];
        }
        if (!data.connected) {
                if (peri) {
                    [centManager connectPeripheral:peri options:nil];
                }
            }
    }
    
}

////Update
-(void)updatePairedDeviceAt:(NSInteger)index with:(RemoteData*)device{
    //Modify Array
    [pairedDevice replaceObjectAtIndex:index withObject:device];
    
    //Save to file
    [DataManager saveRemoteDatas:pairedDevice];
}


////Searching

///////////////////////////////////////////////////////////////////////////////////
//While timeout will call "peripherialsFound:" else will call "peripherialsNotFound"
-(void)searchPeripheralWithTimeout:(NSTimeInterval)timeout{
    if(!searching){
        searching = 1;
        [peripherals removeAllObjects];
        [centManager scanForPeripheralsWithServices:nil options:nil];
        [self performSelector:@selector(returnPeripherals) withObject:nil afterDelay:timeout];
    }
}

-(void)returnPeripherals{
    //Call Delegate to Handle
    if ([peripherals count]) {
        [[self delegate]peripherialsFound:peripherals];
    }else{
        [[self delegate]peripherialsNotFound];
    }
    //Turn Off Searching
    [centManager stopScan];
    
    //Turn Off Searching Flag
    searching =0;
}



////Connection
-(void)connectPeripheral:(CBPeripheral*)peripheral{
    [centManager connectPeripheral:peripheral options:nil];
}


-(void)notifyConnectToPeripherial:(CBPeripheral*)peripheral_connect{
    
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    notification.alertBody = [NSString stringWithFormat:@"已連線:%@",peripheral_connect.name] ;
    notification.alertAction = @"開門";
    notification.category = @"category_ID";
    notification.fireDate = nil;
    NSString *deviceUUID = [peripheral_connect.identifier UUIDString];
    NSInteger deviceCategory = -1;
    for (RemoteData *device in pairedDevice) {
        if ([device.UUIDstring isEqualToString:deviceUUID]) {
            deviceCategory = [device.category integerValue];
        }
    }
    
    switch (deviceCategory) {
        case 1:
            //Door
            break;
        case 2:
            //Scooter
            
            break;
        default:
            break;
    }
    notification.userInfo = [[NSDictionary alloc]initWithObjectsAndKeys:deviceUUID,@"deviceUUID", nil];
    
    
    [[UIApplication sharedApplication]scheduleLocalNotification:notification];
}

-(void) connectToDeviceNumber:(NSInteger)index{
    
    [self searchPeripheralWithTimeout:1];
    RemoteData *data  = [pairedDevice objectAtIndex:index];
    CBPeripheral *peri ;
    if(data.UUIDstring !=nil){
        NSUUID *identifier = [[NSUUID alloc]initWithUUIDString:data.UUIDstring];
        NSArray *returnPeris =[centManager retrievePeripheralsWithIdentifiers:[NSArray arrayWithObject:identifier]];
        if ([returnPeris count])
            peri = [returnPeris objectAtIndex:0];
    }
    [self connectPeripheral:peri];
    
}

-(void)disconnectAllDevice{
    //Cancel Connection to all peripherals
    for(CBPeripheral *per in retreivedPeripherials){
        [centManager cancelPeripheralConnection:per];
    }
}



//Function
-(void)forgetAllDevice{
    //Disconnect to All Peripherals
    [self disconnectAllDevice];
    
    //Clean the data storge on flash
    [DataManager cleanDatas];

}


-(void)unlockScooterWithUUID:(NSString*)scooterUUID{
    //Get Charateristic
    uint8_t cmdUnlock[1]  = {0};
    CBPeripheral *scooterPeripheral = [self peripheralWithUUID:scooterUUID];
    CBCharacteristic *char1 = [[[scooterPeripheral.services firstObject] characteristics]firstObject];
    NSData *data = [[NSData alloc]initWithBytes:cmdUnlock length:1];
    [scooterPeripheral writeValue:data forCharacteristic:char1 type:CBCharacteristicWriteWithResponse];

}
-(void)lockScooterWithUUID:(NSString*)scooterUUID{
    //Get Charateristic
    uint8_t cmdLock[1]  = {1};
    CBPeripheral *scooterPeripheral = [self peripheralWithUUID:scooterUUID];
    CBCharacteristic *char1 = [[[scooterPeripheral.services firstObject] characteristics]firstObject];
    NSData *data = [[NSData alloc]initWithBytes:cmdLock length:1];
    [scooterPeripheral writeValue:data forCharacteristic:char1 type:CBCharacteristicWriteWithResponse];

}



//Manage BLE Callback

-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
    //Check whether exist before adding
    if (![peripherals containsObject:peripheral]) {
        [peripherals addObject:peripheral];
    }
    
    
}

-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    [[self delegate]connectedToPeripheralFail:peripheral];
}

-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    
    //Discover Service
    peripheral.delegate = self;
    [peripheral discoverServices:[NSArray arrayWithObject:uuid_service]];
    //[peripheral discoverServices:nil];
    
    //Flag
    BOOL exist = 0;
    //Check if already paired before adding
 
        for (RemoteData *data in pairedDevice) {
            if (![data.UUIDstring compare:[peripheral.identifier UUIDString]]) {
                exist = 1;
                data.connected = 1;
                break;
            }
        }
    if (!exist) {
        
        
        //Make Peripheral into RemoteDate
        RemoteData *newData = [[RemoteData alloc]init];
        newData.name = peripheral.name;
        newData.UUIDstring = [peripheral.identifier UUIDString];
        newData.peripheral = peripheral;
        newData.autoConnect = 1;
        newData.connected = 1;
        
        unsigned char randKeyBuffer[16]; //128bit
        arc4random_buf(randKeyBuffer,sizeof(randKeyBuffer));
        NSString *aesKeyHexString = [[NSData dataWithBytes:randKeyBuffer length:sizeof(randKeyBuffer)]description];
        newData.aesKeyIV = aesKeyHexString;

        //newData.category = *valueBuffer;
        
        
        [pairedDevice addObject:newData];
        
//TODO
        //Save pairedDevice to file
        [DataManager saveRemoteDatas:pairedDevice];
        
        
        //Update retrieved device
        [retreivedPeripherials addObject:peripheral];
    }
    
    
    //Show notification
    [self notifyConnectToPeripherial:peripheral];
    
    //Call Delelgate to manage connection
    [[self delegate]connectedToPeripheral];
}
-(void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    
    //Setting Data property to disconnected
    int count= 0;
    NSMutableArray *tempArray = [[NSMutableArray alloc]initWithArray:pairedDevice];
    for (RemoteData *data in pairedDevice) {
        if ([data.UUIDstring isEqualToString: peripheral.identifier.UUIDString]){
            data.connected = 0;
            [tempArray replaceObjectAtIndex:count withObject:data];
        }
        count++;
    }
    [pairedDevice replaceObjectsInRange: NSMakeRange (0, [tempArray count]) withObjectsFromArray:tempArray];
    
    
    //Save data to Flash
    [DataManager saveRemoteDatas:pairedDevice];
    
    //Call Delegate
    [[self delegate]disconnectedToPeripherial:peripheral];
}


-(void)centralManagerDidUpdateState:(CBCentralManager *)central{
    if (central.state ==CBCentralManagerStatePoweredOn) {
        BLE_ON =1;
    }else{
        BLE_ON =0;
    }
    
}


-(NSArray*)getRemoteDatas{
    
    //Update pairedDevice from File
    pairedDevice = [[NSMutableArray alloc]initWithArray:[DataManager retrieveRemoteDatas]];
    return  (NSArray *)pairedDevice;
}


-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    if(peripheral.services){
        
        [peripheral discoverCharacteristics:[NSArray arrayWithObjects:[CBUUID UUIDWithString:@"FFF1"],[CBUUID UUIDWithString:@"FFF2"],nil] forService:[peripheral.services objectAtIndex:0]];
    }
}
-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    
    
    //Put Characteristic 2 Value to remote data
    CBCharacteristic *char2 = [[service characteristics]objectAtIndex:1];
    [peripheral readValueForCharacteristic:char2];
    
    

    //NSLog(@"%@",[service.characteristics firstObject]);
    //NSLog(@"%@",char2);
}



-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    int value = 0;
    
    
    if (characteristic.value.bytes){
        value = ((uint8_t*)characteristic.value.bytes)[0];
        //Check which UUID Match and change the category value
        for (int count=0;count<[pairedDevice count];count++) {
            RemoteData *data = [pairedDevice objectAtIndex:count];
            if ([data.UUIDstring isEqualToString:characteristic.service.peripheral.identifier.UUIDString]){
                data.category = [NSNumber numberWithInt:value];
                [pairedDevice replaceObjectAtIndex:count withObject:data];
            }
            
        }
        //Save the data to flash drive
        [DataManager saveRemoteDatas:pairedDevice];
    }else{
        NSLog(@"Can't Read 2nd Char");
    }

}
-(CBPeripheral*)peripheralWithUUID:(NSString *)uuidString{
    for (CBPeripheral *peri in self.retreivedPeripherials) {
        if ([[peri.identifier UUIDString]isEqualToString:uuidString]) {
            return peri;
        }
    }
    
    //No result
    return nil;

}

// appDelegate  Delegate
-(void)appWillTerminate{
    [DataManager saveRemoteDatas:pairedDevice];
    //[self writeDataToFile];
}
-(void)applicationDidEnterBackground{
    //Turn on BLE to search Peripherial
    [centManager scanForPeripheralsWithServices:nil options:nil];
    
    centManager.delegate = self;
    
    [DataManager saveRemoteDatas:pairedDevice];
    
}




@end
