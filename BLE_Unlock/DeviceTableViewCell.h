//
//  DeviceTableViewCell.h
//  BLE_Unlock
//
//  Created by Brian on 2014/11/30.
//  Copyright (c) 2014年 Brian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>


@protocol CellActionDelegate <NSObject>

@required
-(void)pairButtonDidClicked:(NSInteger)index;

@end

@interface DeviceTableViewCell : UITableViewCell
@property id <CellActionDelegate> delegate;


@property (strong,nonatomic) CBPeripheral *selectedPeripherial;
@property (strong,nonatomic) NSUUID* uuid;
@property NSInteger index;
@property BOOL addedAlready;

-(void)updateCell;

@end
