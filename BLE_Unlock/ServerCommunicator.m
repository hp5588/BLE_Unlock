//
//  ServerCommunicator.m
//  BLE_Unlock
//
//  Created by Brian on 2015/8/17.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import "ServerCommunicator.h"
#import "serverAPIDefine.h"
@interface ServerCommunicator ()
@property NSString* ID;
@property NSString* password;
@property NSURLSession *urlSession;
@property BOOL logined;
@property NSMutableURLRequest *req;
@end

@implementation ServerCommunicator

@synthesize delegate;

@synthesize ID;
@synthesize password;
@synthesize urlSession;
@synthesize req;


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.ID = @"";
        self.password = @"";
        req = [[NSMutableURLRequest alloc]init];
        req.HTTPMethod = @"POST";
        req.URL = [NSURL URLWithString:SERVER_LINK];
        urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:nil];
    }
    return self;
}

-(id)init:(NSString*)ID password:(NSString*)password{
    self.ID = ID;
    self.password = password;
    
    urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:nil];
    req = [[NSMutableURLRequest alloc]init];
    req.HTTPMethod = @"POST";
    req.URL = [NSURL URLWithString:SERVER_LINK];
    

    return self;
}


-(void)aesKeyPush:(NSString *)ivKeyString bleUUID:(NSString*)bleUUID{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:ivKeyString forKey:@"aes_key_iv"];
    [dict setObject:bleUUID forKey:@"ble_uuid"];
    NSString *postString = [self postStringWithreq:REQ_AES_KEY_PUSH_HANDLE others:dict];
    NSData *postData = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    req.HTTPBody = postData;
    [[urlSession dataTaskWithRequest:req]resume];
    
}
-(void)checkVerification {
    NSString *postString = [self postStringWithreq:1 others:nil];
    NSData *postData = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    req.HTTPBody =postData;
    [[urlSession dataTaskWithRequest:req] resume];

    
}

-(void)apnsTokenPushToServer:(NSString *)token{
    NSDictionary *tokenDict = [[NSDictionary alloc]initWithObjectsAndKeys:token,@"apns_token", nil];
    
    NSString *postString = [self postStringWithreq:REQ_TOKEN_PUSH others:tokenDict];
    req.HTTPBody = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    [[urlSession dataTaskWithRequest:req] resume];
}

-(void)fbTokenPushToServer:(NSString *)token{
    NSDictionary *tokenDict = [[NSDictionary alloc]initWithObjectsAndKeys:token,@"fb_token", nil];
    NSString *postString = [self postStringWithreq:REQ_USER_REGISTER others:tokenDict];
    req.HTTPBody = [postString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    [[urlSession dataTaskWithRequest:req] resume];
}



-(NSString *)postStringWithreq:(NSInteger)reqNum others:(NSDictionary*)others{
    NSString *basicString = [NSString stringWithFormat:@"id=%@&password=%@&req=%li",self.ID,self.password,(long)reqNum];
   
    NSArray *keys = [others allKeys];
    for (NSString *key in keys) {
        NSString *addOnString = [NSString stringWithFormat:@"&%@=%@",key,[others objectForKey:key]];
        basicString = [basicString stringByAppendingString:addOnString];
    }
    
//TODO: finish reading dictionary
    return basicString;
    
}
-(void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data{
    NSError *error;
    NSDictionary *returnedJsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error: &error];
    NSInteger responseVal = [[returnedJsonDictionary objectForKey:@"response"]integerValue];
    NSDictionary *dataDict = [returnedJsonDictionary objectForKey:@"data"];
    [self.delegate serverResponseWith:responseVal datas:dataDict];
    
}
@end
