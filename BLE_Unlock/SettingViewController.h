//
//  SettingViewController.h
//  BLE_Unlock
//
//  Created by Brian on 2015/8/19.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface SettingViewController : UIViewController<FBSDKLoginButtonDelegate>

@end
