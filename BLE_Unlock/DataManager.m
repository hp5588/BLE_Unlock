//
//  DataManager.m
//  BLE_Unlock
//
//  Created by Brian on 2015/1/25.
//  Copyright (c) 2015年 Brian. All rights reserved.
//


#import "DataManager.h"
@implementation DataManager

+(void)updatePairedDeviceInFileAt:(NSInteger)index with:(RemoteData*)device{
    NSMutableArray *tempArray = [[DataManager retrieveRemoteDatas]copy];
    if ([tempArray count]>index) {
        [tempArray replaceObjectAtIndex:index withObject:device];
        [DataManager saveRemoteDatas:tempArray];
    }else{
        NSLog(@"File Update Fail:device array lengh is not acceptable");
    }
}

+(void)saveRemoteDatas:(NSArray*)datas{
    NSString *path = [DataManager getFilePathAppendingName:@"/PairedDevice.dat"];
    BOOL result = [NSKeyedArchiver archiveRootObject:datas toFile:path];
    NSLog(@"%@",path);
    NSLog(@"Saving result: %d",result);
}
+(NSArray*)retrieveRemoteDatas{
    NSArray *datas = [[NSArray alloc]init];
    NSString *path = [DataManager getFilePathAppendingName:@"/PairedDevice.dat"];
    datas = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    
    if (datas)
        return datas;
    else
        return nil;
    
}


+(NSString*)getFilePathAppendingName:(NSString*)fileName{
    NSArray *systemPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *path = [systemPaths firstObject];
    if (fileName) {
        path = [path stringByAppendingString:fileName];
    }
    return path;
}

+(void)cleanDatas{
    NSString *path = [self getFilePathAppendingName:@"/PairedDevice.dat"];
    NSArray *emptyArray = [[NSArray alloc]init];
    BOOL result = [NSKeyedArchiver archiveRootObject:emptyArray toFile:path];
    if (result)
        NSLog(@"Data Cleaned");
    else
        NSLog(@"Clean Failed");
}


@end
