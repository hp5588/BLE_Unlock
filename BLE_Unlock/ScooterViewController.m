//
//  ScooterViewController.m
//  BLE_Unlock
//
//  Created by Brian on 2015/4/13.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import "ScooterViewController.h"
#import "EncryptionManager.h"

@interface ScooterViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageLock;
@property NSNumber *locked;
@property CBCharacteristic *char1;
@property CBPeripheral *peripherial_scooter;

@end


@implementation ScooterViewController
@synthesize peripherial_scooter;
@synthesize bleController;
@synthesize imageLock;
@synthesize locked;
@synthesize char1;
@synthesize deviceData;

-(void)viewWillAppear:(BOOL)animated{
    //Set Title
    self.title = deviceData.name;
    
    //Set Peripheral
    self.peripherial_scooter = [bleController peripheralWithUUID:deviceData.UUIDstring];
    
    //Initial Varibles
    locked = deviceData.locked;
    
    //Show corresponding image
    [self showImageNumber:[locked intValue]];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //Add tap detector
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureHandle)];
    tapRec.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRec];
    
    
    
}

-(void)tapGestureHandle{
    //Get Charateristic
    uint8_t cmdUnlock[1]  = {0};
    uint8_t cmdLock[1]  = {1};
    
    
    
    if (locked){
        //Do Unlock
        [self showImageNumber:0];
        NSData *data = [[NSData alloc]initWithBytes:cmdUnlock length:1];
        if (char1) {
            [peripherial_scooter writeValue:data forCharacteristic:char1 type:CBCharacteristicWriteWithResponse];
            locked = 0;
        }
        
        
    }else{
        //Lock
        [self showImageNumber:1];
        NSData *data = [[NSData alloc]initWithBytes:cmdLock length:1];
        if (char1) {
            [peripherial_scooter writeValue:data forCharacteristic:char1 type:CBCharacteristicWriteWithResponse];
            locked = [NSNumber numberWithInt:1];
        }
        
        
    }
    
}
-(void)showImageNumber:(NSInteger)index{
    UIImage *img;
    switch (index) {
        case 0:
            img = [UIImage imageNamed:@"unlocked"];
            break;
            
        case 1:
            img = [UIImage imageNamed:@"locked"];
            break;
            
        default:
            break;
    }
    if (img) {
        //Change the imgage on Screen
        imageLock.image = img;
    }

}


-(void)viewDidAppear:(BOOL)animated{
    //Retreive Char1
    char1 = [[[peripherial_scooter.services firstObject] characteristics]firstObject];
    
    //Set image to right place
    imageLock.center = self.view.center;
}
-(void)viewWillDisappear:(BOOL)animated{
    
    ///Save the current state to file
    NSMutableArray * datas = [[NSMutableArray alloc]initWithArray:[bleController getRemoteDatas]];

    NSInteger index = [RemoteData indexOfMatchUUIDofRemoteData:deviceData inArray:datas];
    deviceData.locked = locked;
    [datas replaceObjectAtIndex:index withObject:deviceData];
    [DataManager saveRemoteDatas:datas];
    
}
- (IBAction)encryptAction:(id)sender {
    
    char testString[] = "abcdefg";
    NSData *inData = [NSData dataWithBytes:testString length:sizeof(testString)];
    NSData *encData = [EncryptionManager encryptDataFromData:inData for:deviceData];
    NSLog(@"%@",encData);
    
//        
//    EncPacket packet;
//    memcpy(packet.ble_uuid,"1234567890123456",sizeof(packet.ble_uuid));
//    packet.cmd = 1;
//    memcpy(packet.time,"abcde",sizeof(packet.time));
//    NSData *plainData = [NSData dataWithBytes:&packet length:sizeof(packet)];
//    
//    
//    NSError *errorEn;
//    NSData *encryptedData = [RNEncryptor encryptData:plainData
//                                        withSettings:kRNCryptorAES256Settings
//                                            password:@"abcdddd"
//                                               error:&errorEn];
//    
//    char ivData[16];
//    NSRange ivRange;
//    ivRange.length = 16;
//    ivRange.location = 18;
//    [encryptedData getBytes:ivData range:ivRange];
//    
//    NSRange contentRange;
//    char contentData[encryptedData.length - 32-34];
//    contentRange.location = 34;
//    contentRange.length = encryptedData.length - 32-34;
//    [encryptedData getBytes:contentData range:contentRange];
//    
//    char saltData[8];
//    NSRange saltRange;
//    saltRange.length = 8;
//    saltRange.location = 2;
//    [encryptedData getBytes:saltData range:saltRange];
//    
//    NSData *saltD = [NSData dataWithBytes:saltData length:sizeof(saltData)];
//    NSLog(@"salt: %@",saltD);
//    
//    NSData *contentD = [NSData dataWithBytes:contentData length:sizeof(contentData)];
//    NSLog(@"content: %@",contentD);
//    
//    NSData *ivD = [NSData dataWithBytes:ivData length:sizeof(ivData)];
//    NSLog(@"iv: %@",ivD);
//    
//    NSError *errorDe;
//
//    NSData *decryptedData = [RNDecryptor decryptData:encryptedData
//                                        withPassword:@"abcdddd"
//                                               error:&errorDe];
//    
//    NSLog(@"%@",encryptedData);
//    NSLog(@"%@",decryptedData);
//
//    
//    
//    
//    NSLog(@"size of EncPacket:%lu",sizeof(EncPacket));
    
//    StringEncryption *encryptor = [[StringEncryption alloc]init];
//    StringEncryption *encryptor2 = [[StringEncryption alloc]init];

    //NSData *randIV = [encryptor generateRandomIV:16];
    //NSString *ivString = [[NSString alloc]initWithData:randIV encoding:ns];
//    NSString *ivString = @"acb244124";
//    NSString *hashPass = [encryptor sha256:@"thePassword" length:16];//128bit
//    NSLog(@"Hash: %@",hashPass);
//    NSLog(@"IV String: %@",ivString);
//    
//    NSData *plainData = [NSData dataWithBytes:&packet length:sizeof(packet)];
//    NSData *encData = [encryptor encrypt:plainData key:hashPass iv:ivString ];
//    NSData *decryptedData = [encryptor2 decrypt:encData key:hashPass iv:ivString ];
//    NSLog(@"%@",plainData);
//    NSLog(@"%@",encData);
//    NSLog(@"%@",decryptedData);

    

//    NSData *encData = [EncryptionManager encryptString:plainData for:deviceData];
//    NSData *decryptedData = [EncryptionManager decryptString:encData for:deviceData];
    }



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
