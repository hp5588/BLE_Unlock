//
//  ImageManager.m
//  BLE_Unlock
//
//  Created by Brian on 2015/1/22.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import "ImageManager.h"

@interface ImageManager ()

@end

@implementation ImageManager

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sourceType = UIImagePickerControllerCameraDeviceRear;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
