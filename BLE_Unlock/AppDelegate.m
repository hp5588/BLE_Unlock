//
//  AppDelegate.m
//  BLE_Unlock
//
//  Created by Brian on 2014/9/15.
//  Copyright (c) 2014年 Brian. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    UIUserNotificationType type = UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert;
    
    
    //Unclock Action
    UIMutableUserNotificationAction *actionUnlock = [[UIMutableUserNotificationAction alloc]init];
    actionUnlock.title = @"unclock";
    actionUnlock.identifier = @"UNLOCK_ACTION";
    actionUnlock.activationMode = UIUserNotificationActivationModeBackground;
    actionUnlock.authenticationRequired = NO;
    
    //Add Action into array
    NSArray *actions = [[NSArray alloc]initWithObjects:actionUnlock, nil];
    
    UIMutableUserNotificationCategory *category = [[UIMutableUserNotificationCategory alloc]init];
    category.identifier = @"category_ID";
    [category setActions:actions forContext:UIUserNotificationActionContextDefault];
    NSSet *actionSet = [[NSSet alloc]initWithObjects:category, nil];
    
    UIUserNotificationSettings *setting = [UIUserNotificationSettings settingsForTypes:type categories:actionSet];
    
    [[UIApplication sharedApplication]registerUserNotificationSettings:setting];
    [[UIApplication sharedApplication]registerForRemoteNotifications];
    
    
    
    // Override point for customization after application launch.
     
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    //NSLog(@"-----------Notification Registered------------");

    
}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{


}
-(void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler{
    NSDictionary *detailInfo = [notification userInfo];
    NSString *deviceUUID = [detailInfo objectForKey:@"deviceUUID"];
    NSDictionary *identifierInfo = [[NSDictionary alloc]initWithObjectsAndKeys:
                                    identifier,@"identifier",
                                    deviceUUID,@"deviceUUID",
                                    nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ButtonActionNotification" object:self userInfo:identifierInfo];

}



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSUserDefaults *userD= [NSUserDefaults standardUserDefaults];
    NSString *tokenString = [deviceToken description];
    tokenString = [tokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    tokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    [userD setObject:tokenString forKey:@"APNS_TOKEN"];
    [userD synchronize];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[self delegate]applicationDidEnterBackground];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[self delegate]appWillTerminate];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
