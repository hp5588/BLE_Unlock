//
//  RemoteData.h
//  BLE_Unlock
//
//  Created by Brian on 2014/11/28.
//  Copyright (c) 2014年 Brian. All rights reserved.
//
#import <CoreBluetooth/CoreBluetooth.h>
#ifndef BLE_Unlock_RemoteData_h
#define BLE_Unlock_RemoteData_h

typedef struct encPacket{
    uint8_t cmd;
    char ble_uuid[16];
    char time[5];
}EncPacket;

@interface RemoteData:NSObject<NSCoding>
@property NSString *name;
@property UIImage *photo;
@property NSString *password;
@property BOOL autoConnect;
@property NSString *UUIDstring;
@property CBPeripheral *peripheral;
@property NSString *aesKeyIV;
@property NSString *aesKey128;
@property BOOL connected;

@property NSNumber *category;
@property NSNumber *locked;

+(NSInteger)indexOfMatchUUIDofRemoteData:(RemoteData*)remoteData inArray:(NSArray*)remoteDataArray;



@end


#endif
