//
//  DataManager.h
//  BLE_Unlock
//
//  Created by Brian on 2015/1/25.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RemoteData.h"

@interface DataManager : NSObject
+(void)updatePairedDeviceInFileAt:(NSInteger)index with:(RemoteData*)device;
+(void)saveRemoteDatas:(NSArray*)datas;
+(NSArray*)retrieveRemoteDatas;
+(void)cleanDatas;

@end
