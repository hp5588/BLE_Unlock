//
//  serverAPIDefine.h
//  BLE_Unlock
//
//  Created by Brian on 2015/8/17.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#ifndef BLE_Unlock_serverAPIDefine_h
#define BLE_Unlock_serverAPIDefine_h
#define SERVER_LINK @"https://ks-machining.com:8001/ble_php/https_handler.php"
#define REQ_LOGIN_HANDLE                    1
#define REQ_TOKEN_PUSH                      2
#define REQ_USER_REGISTER                   3
#define REQ_LEND_PUSH_HANDLE                10
#define REQ_AES_KEY_PULL_HANDLE             20
#define REQ_AES_KEY_PUSH_HANDLE             21
#define REQ_AES_KEY_CHECK_EXISTENCE_HANDLE  22
#define REQ_APNS_PUSH_MESSAGE  50



#define RESPTYPE_ACK        0
#define RESPTYPE_PASSVERI   1
#define RESPTYPE_USER_REGISTER_SUCCEED      3
#define RESPTYPE_SUCCEED    5
#define RESPTYPE_KEYPULL    10
#define RESPTYPE_KEYPUSH_SUCCEED 21
#define RESPTYPE_KEY_EXIST 22
#define RESPTYPE_APNS_PUSH_MESSAGE 50

#define RESPTYPE_ERR_APNS_TOKEN -RESPTYPE_TOKEN_PUSH_SUCCEED
#define RESPTYPE_ERR_KEYPUSH    -RESPTYPE_KEYPUSH_SUCCEED
#define RESPTYPE_ERR_KEY_EXIST  -RESPTYPE_KEY_EXIST
#define RESPTYPE_ERR_USER_REGISTER -RESPTYPE_USER_REGISTER_SUCCEED

#endif
