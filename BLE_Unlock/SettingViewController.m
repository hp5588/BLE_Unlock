//
//  SettingViewController.m
//  BLE_Unlock
//
//  Created by Brian on 2015/8/19.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController()

@end

@implementation SettingViewController


-(void)viewDidLoad{
//    self.edgesForExtendedLayout=UIRectEdgeNone;
//    self.extendedLayoutIncludesOpaqueBars=NO;
//    self.automaticallyAdjustsScrollViewInsets=NO;
    
    
    
}
-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{
    //FBSDKLoginManager *lg = [[FBSDKLoginManager alloc]init];
    //[lg logOut];
}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    //Disconnect to all device
    NSDictionary *statusDict = [[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],@"login", nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"LoginStatusChange" object:nil userInfo:statusDict];
    
    //Return to login page
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
