//
//  GlobalVariablesManager.h
//  BLE_Unlock
//
//  Created by Brian on 2015/8/20.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalVariablesManager : NSObject


+(void)saveString:(NSString*)dataString withName:(NSString*)nameString;
+(NSString*)getStringWithName:(NSString*)nameString;
@end
