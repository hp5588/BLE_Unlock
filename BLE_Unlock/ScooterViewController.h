//
//  ScooterViewController.h
//  BLE_Unlock
//
//  Created by Brian on 2015/4/13.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLEController.h"
#import "DataManager.h"
@interface ScooterViewController : UIViewController

@property BLEController *bleController;
@property RemoteData *deviceData;
@end
