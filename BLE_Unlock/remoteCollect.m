//
//  remoteCollect.m
//  BLE_Unlock
//
//  Created by Brian on 2014/11/28.
//  Copyright (c) 2014年 Brian. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "collectionCell.h"
#import "RemoteViewController.m"
#import "ScooterViewController.h"
#import "BLEController.h"
#import "remoteCollect.h"
#import "DataManager.h"

static NSString *idName = @"cllctID";
@interface remoteCollection()
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property NSTimer *refreshTimer;

@property UIActivityIndicatorView *actInt;

@property BLEController *bleController;
@property (strong)NSMutableArray *pairedDevices;

//State variable
@property NSInteger viewState;
                    // 0 In CellView
                    // 1 In RemoteView
                    // 2 In ScooterView

@property id currentViewController;
@property RemoteData *selectedDevice;

@property ServerCommunicator *server;
@property NSString *ID;
@property NSString *password;
@end

@implementation remoteCollection
@synthesize currentViewController;

@synthesize viewState;

@synthesize bleController;
@synthesize actInt;

@synthesize refreshTimer;

@synthesize collectionView;
@synthesize selectedDevice;

@synthesize pairedDevices;

@synthesize server;


- (void)viewDidLoad{
    
    //Set variable
    self.ID = [GlobalVariablesManager getStringWithName:@"ID"];
    self.password = [GlobalVariablesManager getStringWithName:@"Password"];
    
    
    
    //Initial Activity Indicator
    actInt = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        //Add as subview
        actInt.hidesWhenStopped = true;
        actInt.center = collectionView.center;
        [collectionView addSubview:actInt];
    
    
    
    //Intial Collection View
    [collectionView registerNib:[UINib nibWithNibName:@"remoteCell" bundle:nil] forCellWithReuseIdentifier:idName];
    
    //'Add Button' and 'Clean Button'
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(discoveryPageShow)];
    UIBarButtonItem *cleanButton = [[UIBarButtonItem alloc]initWithTitle:@"Clean" style:UIBarButtonItemStylePlain target:self action:@selector(cleanDatas)];
    
    NSArray *buttonArray = [[NSArray alloc]initWithObjects:addButton,cleanButton, nil];
    self.navigationItem.rightBarButtonItems = buttonArray;
    
    //Initiate Bluetooth Central Manager
    //initail BLE Manager
    if (!bleController){
        bleController = [[BLEController alloc]init];
        [bleController initController];
        bleController.delegate = self;
    }
    
    //Initiate server Communicator
    server = [[ServerCommunicator alloc]init:self.ID password:self.password];
    server.delegate = self;
    
    //Add Observer to BLE Controller
    [bleController addObserver:self forKeyPath:@"BLE_CONTROLLER_CHANGE" options:(NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld) context:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationButtonActionHandle:) name:@"ButtonActionNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationLoginHandle:) name:@"LoginStatusChange" object:nil];
    
    //Initial Variables
    viewState = 0;
    
    //inital timer and fire timer
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateCollectionViewData) userInfo:nil repeats:YES];
    [[self refreshTimer]fire];
    
//////Not Done Yet
    //Retreive known device
    pairedDevices = [[bleController getRemoteDatas]copy];
    
    

    
}
-(void)viewWillAppear:(BOOL)animated{

    
    bleController.delegate = self;
    
    //Update viewState
    viewState = 0;
    
    //Get new Data
    pairedDevices = [[bleController getRemoteDatas] copy];
    [collectionView reloadData];
    
//TODO:
    //Upload Data to Cloud (server)
    [self syncAesKey:pairedDevices];

}


-(void)viewWillDisappear:(BOOL)animated{
    
    //Destroy Timer
    //[refreshTimer invalidate];
}


/////Push AES Key to Server
-(void)syncAesKey:(NSArray*)devices{
    for (RemoteData *device in devices) {
        NSString* bleUUID = device.UUIDstring;
        NSString* aesKeyIV = device.aesKeyIV;
        [server aesKeyPush:aesKeyIV bleUUID:bleUUID];
    }
}



///Selector Function
-(void)cleanDatas{
    [bleController forgetAllDevice];
    pairedDevices = [[bleController getRemoteDatas] copy];
    
    //Refresh cellview
    [collectionView reloadData];
    
}

- (IBAction)settingViewShowAction:(id)sender {
    
    
}


///Update
-(void)updateCollectionViewData{
    [bleController autoConnectToPairedDevice];
}
                    
                    
-(void)discoveryPageShow{
    DiscoverPageController *discoveryPage = [[self storyboard]instantiateViewControllerWithIdentifier:@"DiscoverPage"];
    discoveryPage.bleController = [self bleController];
    discoveryPage.deviceDelegate = self;
    [[self navigationController]pushViewController:discoveryPage animated:YES];
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [pairedDevices count];
    
}
-(CGSize) collectionView:(UICollectionView *)collectionView
layout:(UICollectionViewLayout *)collectionViewLayout
sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize frameSize;
    frameSize.height = 180;
    frameSize.width = 140;
    return frameSize;
}


-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    colletionCell *theCell = [collectionView dequeueReusableCellWithReuseIdentifier:idName forIndexPath:indexPath];
    RemoteData *theDevice =[pairedDevices objectAtIndex:indexPath.row];
    theCell.name.text = [NSString stringWithFormat:@"%@",theDevice.name];
    RemoteData *data = [pairedDevices objectAtIndex:indexPath.row];
    if (data.connected){
        theCell.backgroundColor = [UIColor yellowColor];
        theCell.backgroundView.alpha = 1;
        theCell.contentView.alpha = 1;
    }
    else{
        theCell.backgroundView.alpha = 0.7;
        theCell.contentView.alpha = 0.7;
        theCell.backgroundColor = [UIColor grayColor];
    }
        
    
    return theCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   // RemoteViewController *remoteController = [[self storyboard]instantiateViewControllerWithIdentifier:@"RemotePage"];
    
    //Get specified data
    RemoteData *data = [pairedDevices objectAtIndex:indexPath.row];
    if (data.connected) {
        [self navigateToViewWithDevice:data];
    }
}

-(void)navigateToViewWithDevice:(RemoteData*)deviceData{
    selectedDevice = deviceData;
    switch ([deviceData.category intValue]) {
        case 1:{
            //Update viewState
            viewState = 1;
            [self performSegueWithIdentifier:@"ToDoor" sender:nil];
            
        }
            break;
        case 2:{
            //Update viewState
            viewState = 2;
            [self performSegueWithIdentifier:@"ToScooter" sender:nil];
        }
            break;
        default:
            break;
    }

}

-(void)notificationButtonActionHandle:(NSNotification*)noti{
    NSDictionary *userInfo = [noti userInfo];
    NSString *identifierString = [userInfo objectForKey:@"identifier"];
    NSString *deviceUUID = [userInfo objectForKey:@"deviceUUID"];
    if ([identifierString isEqualToString:@"UNLOCK_ACTION"]) {
        //Simply Unlock the Scooter
        [bleController unlockScooterWithUUID:deviceUUID];
        [bleController lockScooterWithUUID:deviceUUID];

        
    }
}


-(void)notificationLoginHandle:(NSNotification*)notification{
    BOOL login = [[[notification userInfo]objectForKey:@"login"]boolValue];
    if (!login) {
        //Logout
        
        //Kill connection timer
        [refreshTimer invalidate];
        
        //disconnect all device
        [bleController disconnectAllDevice];
    }else{
        //Login Add new timer
        //inital timer and fire timer
        refreshTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateCollectionViewData) userInfo:nil repeats:YES];
        [[self refreshTimer]fire];
    }

}

////Segue delegate
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ToScooter"]) {
        ScooterViewController *destController = [segue destinationViewController];
        destController.bleController = bleController;
        destController.deviceData = selectedDevice;
        currentViewController = destController;

    }else if ([segue.identifier isEqualToString:@"ToDoor"]){
        RemoteViewController *destController = [segue destinationViewController];
        destController.bleController = bleController;
        destController.deviceData = selectedDevice;
        currentViewController = destController;
    }
}


///Server delegate

-(void)serverResponseWith:(NSInteger)response datas:(NSDictionary *)dataDict{
    switch (response) {
        case RESPTYPE_ERR_KEYPUSH:
            //Key push error handle
            
            break;
        case RESPTYPE_KEYPUSH_SUCCEED:
            //Key Push succeed
            break;
            
        default:
            break;
    }
}



////Self defined delegate

-(void)connectedToPeripheral{
    //Refresh collection view after connection
    [collectionView reloadData];
}

-(void)disconnectedToPeripherial:(CBPeripheral*)peripheral{
    
    //[[self navigationController]popToRootViewControllerAnimated:YES];
    //Return to The Root Controller if disconnected
    switch (viewState) {
        case 1:{
            RemoteViewController *controller = currentViewController;
            [controller.navigationController popToRootViewControllerAnimated:YES];
            //Do something here to manage the object
        }
            break;
        case 2:{
            ScooterViewController *controller = currentViewController;
            [controller.navigationController popToRootViewControllerAnimated:YES];
        }

        default:
            break;
    }
    
    //Refresh collection view after connection
    [collectionView reloadData];
}


-(void)beginSearching{
    [actInt startAnimating];
}
-(void)finishSearchingWithConnectedRemoteData:(NSArray *)remoteDatas{
    [actInt stopAnimating];
    pairedDevices = [remoteDatas copy];
}
-(void)peripherialsFound:(NSArray *)peripherials{

}
-(void)peripherialsNotFound{
}
-(void)addPairedDevice:(RemoteData *)connectedDev{
    //Update Data     

}



@end