//
//  RemoteData.m
//  BLE_Unlock
//
//  Created by Brian on 2014/11/28.
//  Copyright (c) 2014年 Brian. All rights reserved.
//
#import "RemoteData.h"
#import <Foundation/Foundation.h>

#define KEY_NAME @"name"
#define KEY_PASSWORD @"password"
#define KEY_CONNECTED @"connected"
#define KEY_UUIDstring @"UUIDstring"
#define KEY_PERIPHERIAL @"Peripherial"
#define KEY_AUTOCONNECT @"autoconnect"
#define KEY_PHOTO @"photo"
#define KEY_CATEGORY @"Category"
#define KEY_LOCKED @"LOCKED"
#define KEY_AES_IV @"KEY_AES_IV"
#define KEY_AES_KEY_128 @"KEY_AES_KEY_128"



@implementation RemoteData
@synthesize name;
@synthesize password;
@synthesize connected;
@synthesize UUIDstring;
@synthesize peripheral;
@synthesize autoConnect;
@synthesize photo;
@synthesize aesKeyIV;
@synthesize aesKey128;

@synthesize category;
@synthesize locked;

-(void)encodeWithCoder:(NSCoder *)aCoder{
    //No "peripheral"in stead we save UUID
    [aCoder encodeObject:name forKey:KEY_NAME];
    [aCoder encodeObject:password forKey:KEY_PASSWORD];
    [aCoder encodeObject:[NSNumber numberWithBool:connected] forKey:KEY_CONNECTED];
    [aCoder encodeObject:UUIDstring forKey:KEY_UUIDstring];
    //[aCoder encodeObject:peripheral forKey:KEY_PERIPHERIAL];
    [aCoder encodeObject:[NSNumber numberWithBool:autoConnect] forKey:KEY_AUTOCONNECT];
    [aCoder encodeObject:photo forKey:KEY_PHOTO];
    [aCoder encodeObject:category  forKey:KEY_CATEGORY];
    [aCoder encodeObject:locked  forKey:KEY_LOCKED];
    [aCoder encodeObject:aesKeyIV  forKey:KEY_AES_IV];
    [aCoder encodeObject:aesKey128  forKey:KEY_AES_KEY_128];

}


-(id)initWithCoder:(NSCoder *)aDecoder{
    
    name = [aDecoder decodeObjectForKey:KEY_NAME];
    password = [aDecoder decodeObjectForKey:KEY_PASSWORD];
    connected = [[aDecoder decodeObjectForKey:KEY_CONNECTED] integerValue];
    UUIDstring = [aDecoder decodeObjectForKey:KEY_UUIDstring];
    //peripheral = [aDecoder decodeObjectForKey:KEY_PERIPHERIAL];
    autoConnect = [[aDecoder decodeObjectForKey:KEY_AUTOCONNECT] integerValue];
    photo = [aDecoder decodeObjectForKey:KEY_PHOTO];
    category  = [aDecoder decodeObjectForKey:KEY_CATEGORY];
    locked = [aDecoder decodeObjectForKey:KEY_LOCKED];
    aesKeyIV = [aDecoder decodeObjectForKey:KEY_AES_IV];
    aesKey128 = [aDecoder decodeObjectForKey:KEY_AES_KEY_128];
    return self;
}

+(NSInteger)indexOfMatchUUIDofRemoteData:(RemoteData*)remoteData inArray:(NSArray*)remoteDataArray{
    NSInteger index=0;
    for (RemoteData *rm in remoteDataArray) {
        if ([rm.UUIDstring isEqual:remoteData.UUIDstring]) {
            return index;
            index++;
        }
        
    }
    return -1;

}
@end