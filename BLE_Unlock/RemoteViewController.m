//
//  RemoteViewController.m
//  BLE_Unlock
//
//  Created by Brian on 2014/11/29.
//  Copyright (c) 2014年 Brian. All rights reserved.
//

#define ERROR_RANGE 10

#import <UIKit/UIKit.h>
#import "RemoteViewController.h"
@interface RemoteViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scollView;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property ImageManagerViewController *theIMGcontroller;




@property CBCharacteristic *characteristic;

@property CGFloat lastContentOffset;

@property BOOL triggered;

@end

@implementation RemoteViewController
@synthesize scollView;
@synthesize photo;
@synthesize theIMGcontroller;

@synthesize lastContentOffset;

@synthesize triggered;

@synthesize bleController;
@synthesize deviceData;
@synthesize peripheral;
@synthesize characteristic;

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    CGFloat errorValue = lastContentOffset - scollView.contentOffset.y;
    
    if ((errorValue<0)&&(fabsf(errorValue)>ERROR_RANGE)){
        //Scroll UP
        if (!triggered) {
            [self doorUp];
            [self showImg:0];
            triggered = 1;
        }
    }else if((errorValue>0)&&(fabsf(errorValue)>ERROR_RANGE)){
        if (!triggered) {
            [self doorDown];
            [self showImg:2];
            triggered = 1;
        }
        
    }
    
    lastContentOffset = 0;
}
- (IBAction)addPhotoAction:(UIBarButtonItem *)sender {
    if (!theIMGcontroller) {
        theIMGcontroller  = [[ImageManagerViewController alloc]init];
        theIMGcontroller.delegate = self;
        theIMGcontroller.view.hidden = YES;
    }
    [self presentViewController:theIMGcontroller animated:YES completion:nil];
    
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    lastContentOffset = scrollView.contentOffset.y;
}



uint8_t commandUp[1] = {0};
uint8_t commandStop[1] = {1};
uint8_t commandDown[1] = {2};

-(void)doorUp{
    if(characteristic)
        [peripheral writeValue:[NSData dataWithBytes:commandUp length:1] forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
}
-(void)doorDown{
    if (characteristic)
        [peripheral writeValue:[NSData dataWithBytes:commandDown length:1] forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
}
-(void)doorStop{
    //NSLog(@"stop");
    //NSLog(@"%@",peripheral.name);
    if(characteristic)
        [peripheral writeValue:[NSData dataWithBytes:commandStop length:1] forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
}

-(void)viewDidLoad{
    

    NSLog(@"ViewDidLoad");
    //Setup photo
    
    
    //Gesture Detector Setup
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tappedHadle:)];
    tapRec.numberOfTapsRequired = 1;
    [[self scollView]addGestureRecognizer:tapRec];
    

}

-(void)viewWillAppear:(BOOL)animated{
    //Set Scroll View delegate
    scollView.delegate = self;
    
    //Extract the Peripherial from RemoteData
    characteristic = [[[peripheral.services firstObject] characteristics]firstObject];
    
    //Set Title
    self.title = deviceData.name;
    
    //Set Peripheral
    self.peripheral = [bleController peripheralWithUUID:deviceData.UUIDstring];
    


}
-(void)viewDidAppear:(BOOL)animated{
    //Get Characteristic
    characteristic = [[[peripheral.services firstObject] characteristics]firstObject];
}

-(void)tappedHadle:(UITapGestureRecognizer*)tapRec{
    //NSLog(@"tappedHadle");
    [self showImg:1];
    [self doorStop];
    if (!triggered) {
     
    }
    
}


-(void)showImg:(NSInteger)index{
    //CGRect viewSize ;
    UIImage *img ;
    switch ((int)index) {
        case 0:
            img = [UIImage imageNamed:@"up_notify"];
            break;
        case 1:
            img = [UIImage imageNamed:@"stop"];
            break;
        case 2:
            img = [UIImage imageNamed:@"down_notify"];
            break;
        default:
            break;
    }
    //Setup imgView
    UIImageView *imgView = [[UIImageView alloc]initWithImage:img];
    CGRect imgRect = imgView.frame ;
    imgRect.size.height = 256;
    imgRect.size.width = 256;
    imgView.frame = imgRect;
    imgView.alpha = 0.5;
    imgView.center = self.view.center;
    
    imgView.contentMode =  UIViewContentModeScaleToFill;
    
    [[self view]addSubview:imgView];
        
    [self performSelector:@selector(removeImgWithAnimation:) withObject:imgView afterDelay:0.2];
    


}
-(void)removeImgWithAnimation:(UIImageView *)theView{
    [UIView beginAnimations:nil context:(__bridge void *)(theView)];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    //[theView setAlpha:0.8];
    [theView setAlpha:0.0];
    [UIView commitAnimations];
    
    //[theView removeFromSuperview];
}


//-----Delegetion------///
-(void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context{
    UIImageView *theView = (__bridge UIImageView *)context;
    [theView removeFromSuperview];
    triggered = 0;
    
}
-(void)disconnectedToPeripherial:(CBPeripheral *)peripherial{
    if(peripherial==peripheral){
        [[self navigationController]popToRootViewControllerAnimated:YES];
    }
}

-(void)imageControllerFinished{
    [self.theIMGcontroller dismissViewControllerAnimated:YES completion:nil];

}



@end
