//
//  remoteCollect.h
//  BLE_Unlock
//
//  Created by Brian on 2014/12/17.
//  Copyright (c) 2014年 Brian. All rights reserved.
//

#ifndef BLE_Unlock_remoteCollect_h
#define BLE_Unlock_remoteCollect_h
#import <Foundation/Foundation.h>
#import "ServerCommunicator.h"
#import "serverAPIDefine.h"
#import "RemoteData.h"
#import "DiscoverPageController.h"
#import "GlobalVariablesManager.h"

@interface remoteCollection:UIViewController<UIApplicationDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,BLEControllerDelegate,deviceSourceDelegate,serverResultDelegate>

@end


#endif
