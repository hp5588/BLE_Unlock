//
//  EncryptionManager.m
//  BLE_Unlock
//
//  Created by Brian on 2015/8/22.
//  Copyright (c) 2015年 Brian. All rights reserved.
//
#import <CommonCrypto/CommonCrypto.h>
#import "EncryptionManager.h"



// For each 128bit block
//  0-11     12-15
//| Data | serialNum |

#define AES_KEY_LENGTH kCCKeySizeAES128
#define DATA_LENGTH 32 //Bytes
@implementation EncryptionManager
+(NSData*)encryptDataFromData:(NSData*)data for:(RemoteData*)device{
    
    //NSString *keyString = device.aesKey128;
    NSString *keyString = @"password";
    NSData *passData = [keyString dataUsingEncoding:NSUTF8StringEncoding];
    
    char keyBuffer[AES_KEY_LENGTH+1];
    char dataIn [DATA_LENGTH];
    char dataOut [DATA_LENGTH+ data.length];
    
    memset(keyBuffer, 0, sizeof(keyBuffer));
    memset(dataIn, 0, sizeof(dataIn));
    memset(dataOut, 0, sizeof(dataOut));
    
    [passData getBytes:keyBuffer length:keyString.length];
    [data getBytes:dataIn length:sizeof(dataIn)];
    
    unsigned long outsize = 0;
    
    CCCryptorStatus status =
    CCCrypt(kCCEncrypt,
            kCCAlgorithmAES128,
            kCCOptionECBMode,
            keyBuffer,
            AES_KEY_LENGTH,
            nil,
            dataIn,
            sizeof(dataIn),
            dataOut,
            sizeof(dataOut),
            &outsize
            );
    if (status==kCCSuccess) {
        return [NSData dataWithBytes:dataOut length:outsize];
    }
    return nil;
    
}
//+(NSData*)decryptString:(NSData*)data for:(RemoteData*)device{
   
//    NSData *passData = [keyString dataUsingEncoding:NSUTF8StringEncoding];
//    
//    char keyBuffer[AES_128+1];
//    char dataIn [AES_128];
//    char dataOut [AES_128+data.length];
//    memset(keyBuffer, 0, sizeof(keyBuffer));
//    memset(dataIn, 0, sizeof(dataIn));
//    memset(dataOut, 0, sizeof(dataOut));
//    
//    [passData getBytes:keyBuffer length:keyString.length];
//    [data getBytes:dataIn length:sizeof(dataIn)];
//    
//    unsigned long outsize = 0;
//    
//    CCCryptorStatus status =
//    CCCrypt(kCCEncrypt,
//            kCCAlgorithmAES128,
//            kCCOptionECBMode,
//            keyBuffer,
//            AES_128,
//            nil,
//            dataIn,
//            sizeof(dataIn),
//            dataOut,
//            sizeof(dataOut),
//            &outsize
//            );
//    if (status==kCCSuccess) {
//        return [NSData dataWithBytes:dataOut length:outsize];
//    }
//    return nil;
//}
@end
