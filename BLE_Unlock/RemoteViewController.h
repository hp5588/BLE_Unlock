//
//  RemoteViewController.h
//  BLE_Unlock
//
//  Created by Brian on 2014/11/29.
//  Copyright (c) 2014年 Brian. All rights reserved.
//

#ifndef BLE_Unlock_RemoteViewController_h
#define BLE_Unlock_RemoteViewController_h
#import "RemoteData.h"
#import "BLEController.h"
#import "ImageManagerViewController.h"

@interface RemoteViewController : UIViewController<UIScrollViewDelegate,BLEControllerDelegate,imageControllerDelegate>//<UIGestureRecognizerDelegate>
@property NSInteger index;
@property BLEController *bleController;
@property RemoteData *deviceData;
@property CBPeripheral *peripheral;
@end



#endif
