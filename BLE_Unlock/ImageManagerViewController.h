//
//  ImageManagerViewController.h
//  BLE_Unlock
//
//  Created by Brian on 2015/1/22.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol imageControllerDelegate <NSObject>

-(void) imageControllerFinished;

@end

@interface ImageManagerViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property id<imageControllerDelegate> delegate;

@end
