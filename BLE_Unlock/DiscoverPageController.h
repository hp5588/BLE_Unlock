//
//  DiscoverPageController.h
//  BLE_Unlock
//
//  Created by Brian on 2014/11/30.
//  Copyright (c) 2014年 Brian. All rights reserved.
//

#import <CoreBluetooth/CoreBluetooth.h>
#import "DeviceTableViewCell.h"
#import "BLEController.h"

#import "RemoteData.h"
@protocol deviceSourceDelegate <NSObject>

-(void)addPairedDevice:(RemoteData*)connectedDev;

@end

@interface DiscoverPageController:UITableViewController<CBCentralManagerDelegate,UITableViewDataSource,UITableViewDelegate,CellActionDelegate,BLEControllerDelegate>
@property BLEController *bleController;
@property id<deviceSourceDelegate> deviceDelegate;
@end


