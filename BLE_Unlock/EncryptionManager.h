//
//  EncryptionManager.h
//  BLE_Unlock
//
//  Created by Brian on 2015/8/22.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RemoteData.h"
@interface EncryptionManager : NSObject

+(NSData*)encryptDataFromData:(NSData*)data for:(RemoteData*)device;
+(NSData*)decryptDataFromData:(NSData*)data for:(RemoteData*)device;

@end
