//
//  GlobalVariablesManager.m
//  BLE_Unlock
//
//  Created by Brian on 2015/8/20.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import "GlobalVariablesManager.h"

@implementation GlobalVariablesManager
+(void)saveString:(NSString*)dataString withName:(NSString*)nameString{
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    [df setObject:dataString forKey:nameString];
    [df synchronize];

}


+(NSString*)getStringWithName:(NSString*)nameString{
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    return [df objectForKey:nameString];

}
@end
