//
//  AppDelegate.h
//  BLE_Unlock
//
//  Created by Brian on 2014/9/15.
//  Copyright (c) 2014年 Brian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>


@protocol appDelegate <NSObject>
@optional
-(void)appWillTerminate;
-(void)applicationDidEnterBackground;

@end


@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property id<appDelegate> delegate;


@property (strong, nonatomic) UIWindow *window;

@end
