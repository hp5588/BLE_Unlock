//
//  DiscoverPageController.m
//  BLE_Unlock
//
//  Created by Brian on 2014/11/30.
//  Copyright (c) 2014年 Brian. All rights reserved.
//
#import <CoreBluetooth/CoreBluetooth.h>
#import <Foundation/Foundation.h>
#import "DiscoverPageController.h"


@interface DiscoverPageController()
@property (weak, nonatomic) IBOutlet UITableView *devicesListTableView;

@end



@implementation DiscoverPageController
@synthesize bleController;
@synthesize devicesListTableView;



-(void)viewWillDisappear:(BOOL)animated{
    //Stop Searching 
    

}
-(void)viewDidLoad{
    //Discover Peripheral Instance if avaliable
    bleController.delegate = self;
    //[bleController searchPeripheralWithTimeout:3];
    

    

    
    //Enable refresh control
    self.refreshControl= [[UIRefreshControl alloc]init];
    self.refreshControl.backgroundColor = [UIColor purpleColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    NSDictionary *formatSetting = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    self.refreshControl.attributedTitle = [[NSAttributedString alloc]initWithString:@"尋找中...." attributes:formatSetting];
    [self.refreshControl addTarget:self action:@selector(refreshHandle) forControlEvents:UIControlEventValueChanged];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}
-(void)viewDidAppear:(BOOL)animated{
    //Refresh Automatically
    [self.refreshControl beginRefreshing];
    [bleController searchPeripheralWithTimeout:1];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    //Clean Peripheral
    [bleController.peripherals removeAllObjects];
    
    [[self tableView]reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];

}

-(void)refreshHandle{
    [self.refreshControl beginRefreshing];
    [bleController searchPeripheralWithTimeout:3];
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [bleController.peripherals count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    DeviceTableViewCell *theCell = [tableView dequeueReusableCellWithIdentifier:@"deviceCell"];
    if (!theCell) {
        theCell = [[[NSBundle mainBundle]loadNibNamed:@"deviceCell" owner:self options:nil] firstObject];
    }
    
    //Get current Peripheral
    CBPeripheral *selectedPeripherial =[bleController.peripherals objectAtIndex:indexPath.row];
    
    //Give cell the selected peripherial
    theCell.selectedPeripherial = selectedPeripherial ;
    
    theCell.delegate = self;
    
    //Set up the cell Properties
        //UUID
            theCell.uuid = selectedPeripherial.identifier;
        //Index
            theCell.index = indexPath.row ;
    
    
    
//TODO:  //Check if addedAlready if show CheckMark
    
         //Gray the pari Button
    
    
    //Update Cell Appereance
    [theCell updateCell];
    
    return theCell;

}

////Delegate in Cell
-(void)pairButtonDidClicked:(NSInteger)index{
    //Make Connection
    [bleController connectPeripheral:[[bleController peripherals]objectAtIndex:index]];
    
    //Clean result
}

/////BLE Delegate

-(void)connectedToPeripheral{
    
    [[self navigationController]popViewControllerAnimated:YES];
}

-(void)connectedToPeripheralFail:(CBPeripheral *)peripherial{
//TODO:
    //Alert User that Connection Fail


}
-(void)peripherialsFound:(NSArray *)peripherials{
    //Reload cell
    [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
    //Stop Reloading Control
    [self.refreshControl endRefreshing];

}

-(void)peripherialsNotFound{
    //Reload cell
    [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];

        [[[UIAlertView alloc]initWithTitle:@"提示" message:@"找不到裝置" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil] show];
    
    //Stop Reloading Control
        [self.refreshControl endRefreshing];

}


-(void)centralManagerDidUpdateState:(CBCentralManager *)central{

}

@end

