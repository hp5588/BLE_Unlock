//
//  DeviceTableViewCell.m
//  BLE_Unlock
//
//  Created by Brian on 2014/11/30.
//  Copyright (c) 2014年 Brian. All rights reserved.
//

#import "DeviceTableViewCell.h"



@interface DeviceTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *checkMark;
@property (weak, nonatomic) IBOutlet UILabel *peripherialNameLable;
@property (weak, nonatomic) IBOutlet UILabel *uuidLable;
@end


@implementation DeviceTableViewCell

//Label
@synthesize peripherialNameLable;
@synthesize uuidLable;

//Property
@synthesize selectedPeripherial;
@synthesize uuid;
@synthesize index;
@synthesize addedAlready;

- (IBAction)pairDevice:(UIButton *)sender {
    //Read from file
    //Add new device into array
    
    //Update the file
    
    //Call the delegate to Handle Pair Function
    [[self delegate]pairButtonDidClicked:index];
    
}

-(void)updateCell{
    peripherialNameLable.text = selectedPeripherial.name;
    uuidLable.text = uuid.UUIDString;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
