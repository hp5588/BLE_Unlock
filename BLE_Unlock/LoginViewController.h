//
//  LoginViewController.h
//  BLE_Unlock
//
//  Created by Brian on 2015/8/15.
//  Copyright (c) 2015年 Brian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerCommunicator.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "GlobalVariablesManager.h"

@interface LoginViewController : UIViewController<NSURLSessionDataDelegate,serverResultDelegate,FBSDKLoginButtonDelegate>

@end
