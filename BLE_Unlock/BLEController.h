//
//  BLEController.h
//  BLE_Unlock
//
//  Created by Brian on 2014/12/3.
//  Copyright (c) 2014年 Brian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "AppDelegate.h"

@protocol BLEControllerDelegate <NSObject>


@optional
-(void)peripherialsFound:(NSArray*)peripherials;
-(void)peripherialsNotFound;
-(void)connectedToPeripheral;
-(void)disconnectedToPeripherial:(CBPeripheral *)peripherial;
-(void)connectedToPeripheralFail:(CBPeripheral *)peripherial;
-(void)beginSearching;
-(void)finishSearchingWithConnectedRemoteData:(NSArray *)remoteDatas;

@end


@interface BLEController : NSObject<CBCentralManagerDelegate,CBPeripheralDelegate,UIApplicationDelegate,appDelegate>
//Delegate

@property id<BLEControllerDelegate> delegate;


//Periperial Found
@property NSMutableArray *peripherals;
@property NSMutableArray *retreivedPeripherials;


//Public Method
-(void)initController;
-(void)searchPeripheralWithTimeout:(NSTimeInterval)timeout;
-(void)autoConnectToPairedDevice;

-(void)connectPeripheral:(CBPeripheral*)peripheral;
-(void)connectToDeviceNumber:(NSInteger)index;
-(void)disconnectAllDevice;
-(void)disableController;
-(NSArray *)getRemoteDatas;

-(CBPeripheral*)peripheralWithUUID:(NSString *)uuidString;

-(void)unlockScooterWithUUID:(NSString*)scooterUUID;
-(void)lockScooterWithUUID:(NSString*)scooterUUID;

//Function 
-(void)forgetAllDevice;

@end




